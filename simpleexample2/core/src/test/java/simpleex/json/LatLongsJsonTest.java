package simpleex.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Iterator;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import simpleex.core.LatLong;
import simpleex.core.LatLongs;
import simpleex.core.MetaData;

public class LatLongsJsonTest {

  private final ObjectMapper objectMapper = new ObjectMapper();

  {
    objectMapper.registerModule(new LatLongsModule());
  }

  protected void assertEqualsIgnoreWhitespace(final String expected, final String actual)
      throws Exception {
    assertEquals(expected, actual.replaceAll("\\s+", ""));
  }

  private LatLong latLong1() {
    return new LatLong(63.1, 12.3);
  }

  private LatLong latLong2() {
    return new LatLong(63.0, 12.4);
  }

  @Test
  public void testLatLongsSerialization() throws Exception {
    final String actualJson = objectMapper.writeValueAsString(new LatLongs(latLong1(), latLong2()));
    final String expectedJson = "[{\"latitude\":63.1,\"longitude\":12.3},"
        + "{\"latitude\":63.0,\"longitude\":12.4}]";
    assertEqualsIgnoreWhitespace(expectedJson, actualJson);
  }

  @Test
  public void testLatLongsDeserialization() throws Exception {
    final String json = "[{\"latitude\":63.1,\"longitude\":12.3}, [63.0,12.4]]";
    final LatLongs latLongs = objectMapper.readValue(json, LatLongs.class);
    assertEquals(2, latLongs.getLatLongCount());
    assertEquals(latLong1(), latLongs.getLatLong(0));
    assertEquals(latLong2(), latLongs.getLatLong(1));
  }

  @Test
  public void testLatLongMetaDataSerialization() throws Exception {
    final LatLong latLong = latLong1();
    final MetaData metaData = latLong.getMetaData();
    metaData.addTags("aTag", "bTag");
    metaData.setProperty("aProperty", "aValue");
    final String actualJson = objectMapper.writeValueAsString(latLong);
    final String expectedJson = "{\"latitude\":63.1,\"longitude\":12.3,"
        + "\"metaData\":{\"tags\":[\"aTag\",\"bTag\"],\"properties\":[{\"name\":\"aProperty\",\"value\":\"aValue\"}]}}";
    assertEqualsIgnoreWhitespace(expectedJson, actualJson);
  }

  @Test
  public void testLatLongsMetaDataSerialization() {
    final LatLong latLong = latLong1();
    final MetaData metaData = latLong.getMetaData();
    metaData.addTags("aTag", "bTag");
    metaData.setProperty("aProperty", "aValue");
    final LatLongs latLongs = new LatLongs(latLong);
    try {
      final String actualJson = objectMapper.writeValueAsString(latLongs);
      final String expectedJson = "[{\"latitude\":63.1,\"longitude\":12.3,"
          + "\"metaData\":{\"tags\":[\"aTag\",\"bTag\"],\"properties\":[{\"name\":\"aProperty\",\"value\":\"aValue\"}]}}]";
      assertEqualsIgnoreWhitespace(expectedJson, actualJson);
    } catch (final JsonProcessingException e) {
      fail();
    } catch (final Exception e) {
      fail();
    }
  }

  @Test
  public void testLatLongsMetaDataDeserialization() throws Exception {
    final String json = "[{\"latitude\":63.1,\"longitude\":12.3,"
        + "\"metaData\":{\"tags\":[\"aTag\",\"bTag\"],\"properties\":[{\"name\":\"aProperty\",\"value\":\"aValue\"}]}}]";
    final LatLongs latLongs = objectMapper.readValue(json, LatLongs.class);
    assertEquals(1, latLongs.getLatLongCount());
    assertTrue(latLongs.getLatLong(0).hasMetaData());
  }

  // relies on a certain order
  private void check(final Iterator<String> it, final String... ss) {
    final Iterator<String> it1 = Arrays.asList(ss).iterator();
    while (it1.hasNext() && it.hasNext()) {
      assertEquals(it1.next(), it.next());
    }
    assertEquals(it1.hasNext(), it.hasNext());
  }

  @Test
  public void testLatLongMetaDataDeserialization() throws Exception {
    final String json = "{\"latitude\":63.1,\"longitude\":12.3,"
        + "\"metaData\":{\"tags\":[\"aTag\",\"bTag\"],\"properties\":[{\"name\":\"aProperty\",\"value\":\"aValue\"},[\"bProperty\",\"bValue\"]]}}";
    final LatLong latLong = objectMapper.readValue(json, LatLong.class);
    final MetaData metaData = latLong.getMetaData();
    assertTrue(metaData.hasTags("aTag", "bTag"));
    assertEquals("aValue", metaData.getProperty("aProperty"));
    assertEquals("bValue", metaData.getProperty("bProperty"));
    check(metaData.tags(), "aTag", "bTag");
    check(metaData.propertyNames(), "aProperty", "bProperty");
  }
}
