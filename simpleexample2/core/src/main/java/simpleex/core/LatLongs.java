package simpleex.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Represents a sequence of LatLong objects.
 */
public class LatLongs implements Iterable<LatLong> {

  private final List<LatLong> latLongs = new ArrayList<>();

  /**
   * Initializes an empty LatLongs object.
   */
  public LatLongs() {}

  /**
   * Initializes a LatLongs object with the provided latitude, longitude pairs.
   *
   * @param latLongsArray the latitude, longitude pairs
   */
  public LatLongs(final double... latLongsArray) {
    addLatLongs(latLongsArray);
  }

  /**
   * Initializes a LatLongs object with the provided LatLong objects.
   *
   * @param latLongs the LatLong objects
   */
  public LatLongs(final LatLong... latLongs) {
    addLatLongs(latLongs);
  }

  /**
   * Initializes a LatLongs object with the provided LatLong objects.
   *
   * @param latLongs the LatLong objects
   */
  public LatLongs(final Collection<LatLong> latLongs) {
    addLatLongs(latLongs);
  }

  @Override
  public Iterator<LatLong> iterator() {
    return latLongs.iterator();
  }

  /**
   * Returns a list of all the LatLong objects in this LatLongs.
   *
   * @return the list of LatLong objects
   */
  public List<LatLong> toList() {
    return new ArrayList<>(latLongs);
  }

  /**
   * Gets the number of LatLong objects.
   *
   * @return the number of LatLong objects
   */
  public int getLatLongCount() {
    return latLongs.size();
  }

  /**
   * Gets the LatLong object at the provided position.
   *
   * @param num the position of the LatLong object
   * @return the LatLong object at the provided position
   */
  public LatLong getLatLong(final int num) {
    return latLongs.get(num);
  }

  /**
   * Sets the LatLong object at the provided position.
   *
   * @param num the position to set
   * @param latLong the LatLong object to set at the provided position
   */
  public void setLatLong(final int num, final LatLong latLong) {
    latLongs.set(num, latLong);
  }

  /**
   * Adds a LatLong object to the end of this LatLongs object.
   *
   * @param latLong the LatLong object to append
   * @return the position of the newly added LatLong object
   */
  public int addLatLong(final LatLong latLong) {
    final int pos = latLongs.size();
    latLongs.add(latLong);
    return pos;
  }

  /**
   * Adds a collection of LatLong object to the end of this LatLongs object.
   *
   * @param latLongs the collection of LatLong objects to append
   * @return the position where the LatLong objects were added
   */
  public final int addLatLongs(final Collection<LatLong> latLongs) {
    final int pos = this.latLongs.size();
    this.latLongs.addAll(latLongs);
    return pos;
  }

  /**
   * Adds a the provided LatLong objects to the end of this LatLongs object.
   *
   * @param latLongs the LatLong objects to add
   * @return the position where the LatLong objects were added
   */
  public final int addLatLongs(final LatLong... latLongs) {
    return addLatLongs(List.of(latLongs));
  }

  /**
   * Adds a the provided latitude, longitude pairs to the end of this LatLongs object.
   *
   * @param latLongsArray the latitude, longitude pairs
   * @return the position where the LatLong objects were added
   */
  public final int addLatLongs(final double... latLongsArray) {
    final Collection<LatLong> latLongs = new ArrayList<>(latLongsArray.length / 2);
    for (int i = 0; i < latLongsArray.length; i += 2) {
      latLongs.add(new LatLong(latLongsArray[i], latLongsArray[i + 1]));
    }
    return addLatLongs(latLongs);
  }

  /**
   * Removes the LatLong object at the provided position.
   *
   * @param num the position to remove
   * @return the removed LatLong object
   */
  public LatLong removeLatLong(final int num) {
    return latLongs.remove(num);
  }
}
