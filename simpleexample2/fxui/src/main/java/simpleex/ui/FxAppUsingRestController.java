package simpleex.ui;

/*
@startuml
class FxAppController
class LatLongs
class BorderPane
class "ListView<LatLong>" as ListView
class "fxmapcontrol.MapBase" as MapBase

BorderPane *--> ListView: "left"
BorderPane *--> MapBase: "center"

FxAppController --> LatLongs: "latLongs"
FxAppController --> MapBase: "mapView"
FxAppController --> ListView: "locationListView"
@enduml
 */

/**
 * The controller for the rest app.
 */
public class FxAppUsingRestController extends AbstractFxAppController {

  public FxAppUsingRestController() {
    setDataAccess(new RestLatLongsDataAccess("http://localhost:8080/latLong", getObjectMapper()));
  }
}
